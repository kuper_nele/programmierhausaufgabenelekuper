package view;

import controller.BunteRechteckeController;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;



public class Zeichenflaeche extends JPanel {
	//Wurde automatisch hinzugefügt => Zeichenfläche
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//
	
	private final BunteRechteckeController CONTROLLER;
				
				public Zeichenflaeche(BunteRechteckeController brc) {
					CONTROLLER = brc;
					
				}
				
				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					this.setBackground(Color.WHITE);
					g.setColor(Color.BLACK);
					g.drawRect(0, 0, 50, 50);
					
						for(int i = 0; i < CONTROLLER.getRechtecke().size();i++) {
							
							g.drawRect(CONTROLLER.getRechtecke().get(i).getX(), 
							CONTROLLER.getRechtecke().get(i).getY(),
							CONTROLLER.getRechtecke().get(i).getBreite(),
							CONTROLLER.getRechtecke().get(i).getHoehe());
							
						}
				}
}
