package test;

import model.Rechteck;
import controller.BunteRechteckeController;


public class RechteckTest {
	

	public static void main(String[] args) {
		
		
		
		// Erstellen von 5 Rechtecken parameterlos, 5 vollparametrisiert
		
	
		
		
		//Parameterlos:
		
			Rechteck rechteck0 = new Rechteck();
		
				rechteck0.setX(10);
				rechteck0.setY(10);
				rechteck0.setBreite(30);
				rechteck0.setHoehe(40);
				
				
			Rechteck rechteck1 = new Rechteck();
			
				rechteck1.setX(25);
				rechteck1.setY(25);
				rechteck1.setBreite(100);
				rechteck1.setHoehe(20);
				
			
			Rechteck rechteck2 = new Rechteck();
			
				rechteck2.setX(260);
				rechteck2.setY(10);
				rechteck2.setBreite(200);
				rechteck2.setHoehe(100);
				
				
			Rechteck rechteck3 = new Rechteck();
			
				rechteck3.setX(5);
				rechteck3.setY(500);
				rechteck3.setBreite(300);
				rechteck3.setHoehe(25);
				
				
			Rechteck rechteck4 = new Rechteck();
			
				rechteck4.setX(100);
				rechteck4.setY(100);
				rechteck4.setBreite(100);
				rechteck4.setHoehe(100);
			
				
		
		// Parametrisiert:
	
		
		
		
			Rechteck rechteck5 = new Rechteck(200,200,200,200);
			
			Rechteck rechteck6 = new Rechteck(800,400,20,20);
			
			Rechteck rechteck7 = new Rechteck(800,450,20,20);
			
			Rechteck rechteck8 = new Rechteck(850,400,20,20);
			
			Rechteck rechteck9 = new Rechteck(855,455,25,25);
			
			

		// Ausgaben + Ausgabe mittels equals f�r toString 
			
			System.out.println(rechteck0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
			
			System.out.println(" Rechteck0 besitzt die Werte:  x = " + rechteck0.getX() + " y = " + rechteck0.getY() + " Breite = "
			        + rechteck0.getBreite() + " H�he = " + rechteck0.getHoehe());
			
			System.out.println(" Rechteck1 besitzt die Werte:  x = " + rechteck1.getX() + " y = " + rechteck1.getY() + " Breite = "
			        + rechteck1.getBreite() + " H�he = " + rechteck1.getHoehe());
			
			System.out.println(" Rechteck2 besitzt die Werte:  x = " + rechteck2.getX() + " y = " + rechteck2.getY() + " Breite = "
			        + rechteck2.getBreite() + " H�he = " + rechteck2.getHoehe());
			
			System.out.println(" Rechteck3 besitzt die Werte:  x = " + rechteck3.getX() + " y = " + rechteck3.getY() + " Breite = "
			        + rechteck3.getBreite() + " H�he = " + rechteck3.getHoehe());
			
			System.out.println(" Rechteck4 besitzt die Werte:  x = " + rechteck4.getX() + " y = " + rechteck4.getY() + " Breite = "
			        + rechteck4.getBreite() + " H�he = " + rechteck4.getHoehe());
			
			
			System.out.println(" Rechteck5 besitzt die Werte:  x = " + rechteck5.getX() + " y = " + rechteck5.getY() + " Breite = "
			        + rechteck5.getBreite() + " H�he = " + rechteck5.getHoehe());
			
			System.out.println(" Rechteck6 besitzt die Werte:  x = " + rechteck6.getX() + " y = " + rechteck6.getY() + " Breite = "
			        + rechteck6.getBreite() + " H�he = " + rechteck6.getHoehe());
			
			System.out.println(" Rechteck7 besitzt die Werte:  x = " + rechteck7.getX() + " y = " + rechteck7.getY() + " Breite = "
			        + rechteck7.getBreite() + " H�he = " + rechteck7.getHoehe());
			
			System.out.println(" Rechteck8 besitzt die Werte:  x = " + rechteck8.getX() + " y = " + rechteck8.getY() + " Breite = "
			        + rechteck8.getBreite() + " H�he = " + rechteck8.getHoehe());
			
			System.out.println(" Rechteck9 besitzt die Werte:  x = " + rechteck9.getX() + " y = " + rechteck9.getY() + " Breite = "
			        + rechteck9.getBreite() + " H�he = " + rechteck9.getHoehe());
			
			

			
		BunteRechteckeController controller = new BunteRechteckeController();
		
		
				controller.add(rechteck0);
				
				controller.add(rechteck1);
				
				controller.add(rechteck2);
				
				controller.add(rechteck3);
				
				controller.add(rechteck4);
				
				controller.add(rechteck5);
				
				controller.add(rechteck6);
				
				controller.add(rechteck7);
				
				controller.add(rechteck8);
				
				controller.add(rechteck9);
				
			
			System.out.println(controller.toString());
			System.out.println(controller.toString().equals("bunte-Rechtecke-Controller: [rechtecke= [Rechteck [x=10, y=10, breite=30, hoehe=40], "
																									+ "Rechteck [x=25, y=25, breite=100, hoehe=20], "
																									+ "Rechteck [x=260, y=10, breite=200, hoehe=100], "
																									+ "Rechteck [x=5, y=500, breite=300, hoehe=25], "
																									+ "Rechteck [x=100, y=100, breite=100, hoehe=100], "
																									+ "Rechteck [x=200, y=200, breite=200, hoehe=200], "
																									+ "Rechteck [x=800, y=400, breite=20, hoehe=20], "
																									+ "Rechteck [x=800, y=450, breite=20, hoehe=20], "
																									+ "Rechteck [x=850, y=400, breite=20, hoehe=20], "
																									+ "Rechteck [x=855, y=455, breite=25, hoehe=25]]] "));
			//false als Ergebnis ;-;
			
	}

}
