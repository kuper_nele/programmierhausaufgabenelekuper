package model;

public class Rechteck {

	

	private int y;
	private int x;
	private int breite;
	private int hoehe;
	
	public Rechteck() {	
		this.x = 0;
		this.y = 0;
		this.breite = 0;
		this.hoehe = 0;
		
	}
	
		public Rechteck (int x, int y, int breite, int hoehe) {
			this.y = y;
			this.x = x;
			this.breite = breite;
			this.hoehe = hoehe;
			}
	
	//neue Methode toString
		
					@Override public String toString() {
						return "Rechteck [x=10, y=10, breite=30, hoehe=40]";
						
					}
	
		
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}
}
