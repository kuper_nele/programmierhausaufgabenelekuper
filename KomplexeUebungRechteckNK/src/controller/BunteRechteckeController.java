package controller;

import model.Rechteck;
import java.util.LinkedList;

public class BunteRechteckeController {
	//Attribut der Klasse
	private LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>(); 
	
	

	public static void main(String[] args) {
	//leer gelassen 

	}
	
//leere LinkedList wird zugewiesen
public BunteRechteckeController() {
		
		rechtecke = new LinkedList<Rechteck>();
		
}


//Methoden werden implementiert
public void add(Rechteck rechteck) {
	
		rechtecke.add(rechteck);
		
}


public void reset() {
	
		rechtecke.clear();
	
}


public LinkedList<Rechteck> getRechtecke(){
	
		return rechtecke;
		
}


@Override public String toString() {
	
	return " bunte-Rechtecke-Controller: [rechtecke = " + rechtecke + " ]";
	
}
	
}
